package com.gabrielgomes.fabrisat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInstaller;
import android.media.MediaCas;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Chamado extends AppCompatActivity  {
    private EditText condominio;
    private EditText problema;
    private EditText descricao;
    private Button enviar;
    private AlertDialog.Builder alerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chamado);

        condominio = (EditText) findViewById(R.id.txtEmail);
        problema = (EditText) findViewById(R.id.txtProblema);
        descricao =(EditText) findViewById(R.id.txtDes);
        enviar = (Button) findViewById(R.id.btEnviar);






        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (condominio.getText().toString().equals("") || problema.getText().toString().equals("") || descricao.getText().toString().equals("") || condominio.getText().toString().equals("") && problema.getText().toString().equals("") && descricao.getText().toString().equals("")) {
                    Toast.makeText(Chamado.this, "Preencha todos os campos", Toast.LENGTH_LONG).show();
                } else {
                    boolean online = isOnline(Chamado.this);
                    if(online==true){
                        sendEmail();
                        condominio.setText("");
                        problema.setText("");
                        descricao.setText("");
                    }else{
                        Toast.makeText(Chamado.this, "Conexão não encontrada", Toast.LENGTH_LONG).show();
                    }




                }
            }
        });

    }









    //Validações ao apertar o botão voltar
    @Override
    public void onBackPressed() {

        if (!condominio.getText().toString().equals("")|| !problema.getText().toString().equals("") || !descricao.getText().toString().equals("") ||!condominio.getText().toString().equals("")&& !problema.getText().toString().equals("") && !descricao.getText().toString().equals("")){
            alerta = new AlertDialog.Builder(Chamado.this);
            alerta.setTitle("Deseja mesmo sair dessa página?");
            alerta.setMessage("Você não terminou seu chamado, deseja sair assim mesmo?");
            alerta.setPositiveButton("Sim",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            alerta.setNegativeButton("Não",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            alerta.create();
            alerta.show();

        }else{
            super.onBackPressed();
        }

    }
    // Enviar email
        public void sendEmail(){
        String email = "fabrisatsbc@gmail.com";
        String assunto = problema.getText().toString().trim();
        String texto = condominio.getText().toString().trim() + "\n \n" + descricao.getText().toString().trim();

        SendEmail sm = new SendEmail(this, email, assunto, texto);
        sm.execute();
        }


        //Verificar conexão
        public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected())
            return true;
        else
            return false;
    }

    }




