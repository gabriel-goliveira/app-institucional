package com.gabrielgomes.fabrisat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Contatos extends AppCompatActivity {
        private Button chamado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contatos);

        chamado = (Button) findViewById(R.id.btChamado);

        chamado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chamado =  new Intent(Contatos.this, Chamado.class);
                startActivity(chamado);
            }
        });
    }
}
