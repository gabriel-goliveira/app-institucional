package com.gabrielgomes.fabrisat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Servico extends AppCompatActivity {
    private TextView servico;
    private TextView descricao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servico);

        servico = (TextView) findViewById(R.id.txtServico);
        descricao = (TextView) findViewById(R.id.txtInfos);

        Bundle extra = getIntent().getExtras();
        if(extra != null) {
            String opcaoEscolhida = extra.getString("servico");
            if(opcaoEscolhida.equals("Antena Coletiva")){
                servico.setText("Antena Coletiva");
                descricao.setText("\b - Antena Parabólica\b\n \n" +
                        "Sistema indicado para locais com recepção de sinais VHF/UHF ruins. Instala-se uma antena parabólica no topo do prédio e um rack no quadro de antenas do condomínio, onde o sinal recebido numa frequência de satélite é modulado para uma frequência de VHF e distribuído através de cabos. A recepção via satélite proporciona uma excelente qualidade de imagem e som, eliminando problemas como fantasmas, interferências, etc... \n \n" +
                        "\b - Antena VHF/UHF\b\n\n" +
                        "Ótima opção para locais com boa recepção de sinais, o sistema VHF/UHF é simples e eficaz, utilizam-se antenas para captar os sinais e misturadores e amplificadores para a equalização dos mesmos. Apresenta as seguintes emissoras nas frequências de VHF Cultura, SBT, Globo, Record, Rede TV, Gazeta e Bandeirantes e nas frequências de UHF Canal 21, Mtv, Rede Vida , Rede Mulher e CBI. Com certeza a melhor relação custo/benefício no segmento de antenas para condomínios. \n");
            }else if(opcaoEscolhida.equals("Centrais de Portaria")){
                servico.setText("Centrais de Portaria");
                descricao.setText("\b - Centrais de Portaria\b\n\n" +
                        "Oferece segurança e conforto para os condôminos, possibilitando toda comunicação entre (visitante - apto), (apto - portaria) e (apto - apto). A central pode ser operada pelo porteiro. Atualmente são encontradas no mercado dois modelos mais comuns de centrais: digital e analógica. Em situações em que o condomínio não tem operador, há uma alternativa de instalar um porteiro eletrônico, permitindo uma comunicação direta com o morador do apartamento, que por sua vez poderá acionar a abertura do portão através do interfone.\n");
            }else if(opcaoEscolhida.equals("Centrais PAX")){
                servico.setText("Centrais PAX");
                descricao.setText("\b - Centrais PAX\b\n\n"+ "PAX é um sistema para comunicação interna entre apartamentos/ condomínios sem linha externa, em outras palavras, interfones. E o PAX por PABX é a central de comunicação mais usado pelas empresas, é interligado por ramais e pode receber chamadas externas\n");
            }else if(opcaoEscolhida.equals("CFTV")){
                servico.setText("CFTV");
                descricao.setText("\b - CFTV \b\n\n" + "Na área de sistemas de monitoramento e segurança eletrônica, CFTV é a sigla utilizada para Circuito Fechado de TV, que teve origem na sigla em inglês para o mesmo serviço, o CCTV (Closed Circuit Television).\n" +"O Circuito Fechado de TV nada mais é do que um sistema de monitoramento interno, realizado através de câmeras distribuídas e conectadas a um sistema central, que disponibiliza as imagens através de monitores assim como realiza a gravação desses registros.\n" +
                        "\n" +
                        "O CFTV é utilizado principalmente para monitoramento e vigilância, visando registrar incidentes de segurança, vandalismo, comportamento indevido e diversas outras ocorrências. No entanto vem sendo muito utilizado também para outros fins, como monitoramento viário, para fins ambientais, comportamentais, segurança do trabalho, entre outros.");
            }else if(opcaoEscolhida.equals("Automatização de Portões")){
                servico.setText("Automatização de Portões");
                descricao.setText("\b - Automatização de Portões\b\n\n" + "Basicamente são três os tipos de portões que podem ser encontrados:\n" +
                        "- Pivotante,\n" +
                        "- Basculante, \n" +
                        "- Deslizante,\n" +
                        "A automatização é feita acoplando-se o motor em um portão já existente, proporcionando o seu acionamento à distância, através de controle remoto, sendo que o tempo de abertura também poderá ser ajustado. \n");
            }else if(opcaoEscolhida.equals("Serralheria")){
                servico.setText("Serralheria");
                descricao.setText("\b - Serralheria \b\n\n" + "Está sob as responsabilidades de um Serralheiro recortar, modelar e trabalhar barras perfiladas de materiais ferrosos e não ferrosos para fabricar esquadrias, portas, grades, vitrais e peças similares, desenvolver produtos e soluções que visam suprir a demanda crescente do mercado corporativo em soluções de espaço, favorecendo o bem estar dos usuários, confeccionar reparar e instalar peças e elementos diversos em chapas de metal como aço, ferro galvanizado, cobre, estanho, latão, alumínio e zinco, executar ajustes, instalação de peças e fazendo medições, auxiliando no recorte e modelação de chapas e barras.");
            }else if(opcaoEscolhida.equals("Controle de Acesso de Veículos")){
                servico.setText("Controle de Acesso de Veículos");
                servico.setTextSize(24F);
                descricao.setText("\b - Controle de Acesso de Veículos \b\n\n" + "Garantir a segurança na entrada e saída de veículos de ambientes que necessitam de controle é o principal objetivo dessa solução. Produtos altamente tecnológicos e inovadores garantem alto índice de inteligência e Seguridade para todos os projetos.\n");
            }else if(opcaoEscolhida.equals("Controle de Acesso de Visitantes")){
                servico.setText("Controle de Acesso de Visitantes");
                servico.setTextSize(24F);
                descricao.setText("\b - Controle de Acesso de Visitantes\b\n\n" + "Garantir a segurança na entrada e saída de Visistantes de ambientes que necessitam de controle é o principal objetivo dessa solução. Produtos altamente tecnológicos e inovadores garantem alto índice de inteligência e Seguridade para todos os projetos.\n");
            }else if(opcaoEscolhida.equals("Iluminação de Emergência")){
                servico.setText("Iluminação de Emergência");
                descricao.setText("\b - Iluminação de Emergência\b\n\n" + "O sistema de iluminação de emergência deve clarear os ambientes de passagem horizontal e vertical com iluminação suficiente para evitar acidentes e garantir a evacuação das áreas de risco, cumprindo o objetivo de proteger a vida das pessoas e facilitar a ação dos bombeiros. ");
            }
        }
    }
}
