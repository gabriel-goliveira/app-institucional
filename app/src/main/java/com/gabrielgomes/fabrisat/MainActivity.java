package com.gabrielgomes.fabrisat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private ImageView sobre;
    private ImageView clientes;
    private ImageView contatos;
    private ImageView servicos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sobre = (ImageView) findViewById(R.id.imgSobre);
        clientes = (ImageView) findViewById(R.id.imgClientes);
        contatos = (ImageView) findViewById(R.id.imgContatos);
        servicos = (ImageView) findViewById(R.id.imgTrabalho);

        sobre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MainActivity.this, Sobre.class);
                startActivity(it);
            }
        });

        clientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent c = new Intent(MainActivity.this,Clientes.class);
                startActivity(c);
            }
        });

        contatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent con = new Intent(MainActivity.this, Contatos.class);
                startActivity(con);
            }
        });
        servicos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ser = new Intent(MainActivity.this, Trabalhos.class);
                startActivity(ser);
            }
        });
    }
}
