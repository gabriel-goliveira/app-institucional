package com.gabrielgomes.fabrisat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Trabalhos extends AppCompatActivity {
    private ListView lista;
    private String servicos[] ={"Antena Coletiva", "Centrais de Portaria", "Centrais PAX", "CFTV", "Automatização de Portões", "Serralheria", "Controle de Acesso de Veículos", "Controle de Acesso de Visitantes", "Iluminação de Emergência"};
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trabalhos);

        lista =  (ListView) findViewById(R.id.lista);
        adapter = new ArrayAdapter<String>(
                Trabalhos.this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                servicos
        );
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int codigoServico = position;
                String valor = lista.getItemAtPosition(codigoServico).toString();
                Intent it = new Intent(Trabalhos.this, Servico.class);
                it.putExtra("servico", valor);
                startActivity(it);


            }
        });
    }
}
